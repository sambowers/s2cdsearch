import argparse
import csv
import datetime
import mgrs
import os
import re
import requests


def _validateTile(tile):
    """
    Validate the name structure of a Sentinel-2 tile. This tests whether the input tile format is correct.
    
    Args:
        tile: A string containing the name of the tile to to download.
    """
    
    # Tests whether string is in format ##XXX
    name_test = re.match("[0-9]{2}[A-Z]{3}$",tile)
    
    return bool(name_test)
    
    
def main(tile, start = '20150101', end = datetime.datetime.today().strftime('%Y%m%d'), maxcloud = 100, level = 'L1C', output_file = ''):
    """main(tile, start = '20150101', end = datetime.datetime.today().strftime('%Y%m%d'), maxcloud = 100, level = 'L1C', output_file = '')
    
    Find Sentinel-2 data on the CreoDias platofrm, specifying a particular tile, date ranges and degrees of cloud cover. This is the function that is initiated from the command line.
    
    Args:
        tile: A string containing the name of the tile to to download.
        start: Start date for search in format YYYYMMDD. Start date may not precede 20161206, the date where the format of Sentinel-2 files was simplified. Defaults to 20161206.
        end: End date for search in format YYYYMMDD. Defaults to today's date.
        maxcloud: An integer of maximum percentage of cloud cover to download. Defaults to 100 %% (download all images, regardless of cloud cover).
        level: Processing level, either 'L1C' or 'L2A',
        output_file: Optionally specify an output file. Defaults to printing to screen.
    """
    
    assert _validateTile(tile), "Tile format not recognised. Tiles should have the format ##XXX, you input '%s'."%tile
    assert len(start) == 8, "Start date (%s) not recognised."%str(start)
    assert len(end) == 8, "End date (%s) not recognised."%str(end)
    assert maxcloud <= 100. and maxcloud > 0, "Maxcloud must be between 0 and 100."
    assert level in ['L1C', 'L2A'], "Level must be 'L1C' or 'L2A'."
    
    startDate = datetime.date(int(start[:4]), int(start[4:6]), int(start[6:]))
    endDate = datetime.date(int(end[:4]), int(end[4:6]), int(end[6:]))
    
    # Get lat/lon of tile
    m = mgrs.MGRS()
    lat, lon = m.toLatLon('%s55'%tile) # 55 to return the point at the centre of the tile
    
    url = 'https://finder.creodias.eu/resto/api/collections/Sentinel2/search.json'

    params = dict(
        maxRecords=str(2000),
        startDate = str(startDate) + 'T00:00:00Z',
        endDate = str(endDate) + 'T00:00:00Z',
        cloudCover = '[%s,%s]'%('0',str(maxcloud)),
        productType = level,
        sortParam = 'startDate',
        sortOrder = 'ascending',
        geometry = 'POINT(%s+%s)'%(str(lon),str(lat)),
        dataset='ESA-DATASET'
    )
    
    # Get search pattern for inputs

    # There is a more elegant way with 'requests', but this is a hack to make requests/CreoDias understand each other
    search_pattern = '%s?maxRecords=%s&startDate=%s&completionDate=%s&endDate=%s&cloudCover=%s&productType=%s&sortParam=%s&sortOrder=%s&geometry=%s&dataset=%s'

    search_string = search_pattern%(url,params['maxRecords'],params['startDate'],params['endDate'],params['endDate'],params['cloudCover'],params['productType'],params['sortParam'],params['sortOrder'],params['geometry'],params['dataset'])

    # Get data
    resp = requests.get(search_string)
    data = resp.json() 
    
    # Extract file paths
    input_files = [image['properties']['productIdentifier'] for image in data['features']]

    # Write to screen    
    if output_file == '':
        for input_file in input_files:
            print input_file
    
    else:
        with open(output_file, 'wb') as outputfile:
            for input_file in input_files:
                outputfile.write('%s\n'%input_file)
    

if __name__ == '__main__':
    '''
    '''
    
    
    # Set up command line parser
    parser = argparse.ArgumentParser(description = 'Get locations of Sentinel-2 data stored on the CreoDias platform.')

    parser._action_groups.pop()
    required = parser.add_argument_group('Required arguments')
    optional = parser.add_argument_group('Optional arguments')

    # Required arguments
    required.add_argument('-t', '--tile', type = str, required = True, help = "Sentinel 2 tile name, in format ##XXX.")
    
    # Optional arguments
    optional.add_argument('-s', '--start', type = str, default = '20150101', help = "Start date for search in format YYYYMMDD. Start date should not precede the launch of Sentinel-2 (2015). Defaults to the entire archive.")
    optional.add_argument('-e', '--end', type = str, default = datetime.datetime.today().strftime('%Y%m%d'), help = "End date for search in format YYYYMMDD. Defaults to today's date.")
    optional.add_argument('-c', '--cloud', type = int, default = 100, metavar = '%', help = "Maximum percentage of cloud cover to download. Defaults to 100 %% (get all images, regardless of cloud cover).")
    optional.add_argument('-o', '--output_file', type = str, metavar = 'PATH', default = '', help = "Write to an output file.")
    optional.add_argument('-l', '--level', type = str, metavar = 'L1C/L2A', default = 'L1C', help = "Specify a processign level (L1C or L2A). Defaults to L1C.")
    
    # Get arguments from command line
    args = parser.parse_args()
    
    # Find the files
    main(args.tile, start = args.start, end = args.end, maxcloud = args.cloud, level = args.level, output_file = args.output_file)    